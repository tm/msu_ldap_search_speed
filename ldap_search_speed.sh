#!/bin/bash
START=$(date +%s)
# do something
# start your script work here
ldapsearch -H ldap://ldap.msu.edu:389 -LLL -b dc=msu,dc=edu -x "(uid=nk)" sn cn mail
# your logic ends here
END=$(date +%s)
DIFF=$(( $END - $START ))
echo "It took $DIFF seconds"
